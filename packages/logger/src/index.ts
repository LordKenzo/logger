export default class Logger {
    message: string = "";

    debug(...message: string[]): void{
        console.debug.apply(this, message);
    }
    log(...message: string[]): void{
        console.log.apply(this, message);
    }
    info(...message: string[]): void{
        console.info.apply(this, message);
    }
    warn(...message: string[]): void{
        console.warn.apply(this, message);
    }
    error(...message: string[]): void{
        console.error.apply(this, message);
    }

    toString(): string{
        return 'Hello from Logger';
    }

    version(): string{
        return '1';
    }

}
